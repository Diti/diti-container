extern crate rand;

use std::process::Command;
use rand::Rng;

static PHRASES: &'static[&'static str] = &[
    "I love owls!",
    "Penguins rule!",
    "Go 42!"
];

fn main() {
    let mut rng = rand::thread_rng();
    let index: usize = rng.next_u32() as usize % PHRASES.len();
    let output = Command::new("figlet")
                         .arg(PHRASES[index])
                         .output()
                         .unwrap_or_else(|e| { panic!("failed to execute process: {}", e) });
    println!("{}", String::from_utf8(output.stdout).unwrap());
}
