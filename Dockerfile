FROM ubuntu:15.04
MAINTAINER Conrad Kleinespel <conradk@conradk.com>

RUN locale-gen en_US.UTF-8 && echo 'LANG="en_US.UTF-8"' > /etc/default/locale

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update -y && apt-get upgrade -y

# Install Figlet, a program that writes with ASCII fonts.
RUN apt-get install -y figlet

# Install Rust and Cargo.
RUN apt-get install -y wget
WORKDIR /tmp
RUN wget -O rust.tar.gz https://static.rust-lang.org/dist/rust-1.0.0-`uname -m`-unknown-linux-gnu.tar.gz
RUN tar -xzvf rust.tar.gz
RUN cd rust-1.0.0-`uname -m`-unknown-linux-gnu && ./install.sh
RUN apt-get install -y gcc

# Add the source files for the Rust binary that will print out the sentences.
ADD . /diti

# Compile the program.
WORKDIR /diti
RUN cargo build

ENTRYPOINT ["/diti/target/debug/diti"]
